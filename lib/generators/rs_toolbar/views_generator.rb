# Thanks to plataformatec/devise
# The code used to inspire this generator!
require 'rails/generators'

module RsToolbar
  module Generators
    class ViewsGenerator < Rails::Generators::Base #:nodoc:
      source_root File.expand_path("../../../../app/views", __FILE__)
      desc "Used to copy rs_toolbar views to your application's views."

      def copy_views
        view_directory :rs_toolbar
      end

    protected

      def view_directory(name)
        directory name.to_s, "app/views/#{name}"
      end
    end
  end
end
