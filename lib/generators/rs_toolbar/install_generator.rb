# encoding: utf-8
require 'rails/generators'

module RsToolbar
  module Generators
    class InstallGenerator < Rails::Generators::Base #:nodoc:
      source_root File.expand_path("../templates", __FILE__)

      desc "Install RsToolbar initializer and assets"
      def install
        copy_file 'config/initializers/rs_toolbar.rb', 'config/initializers/rs_toolbar.rb'
        copy_file 'config/locales/rs_toolbar.ru.yml', 'config/locales/rs_toolbar.ru.yml'
        append_file "app/assets/stylesheets/application.css.scss" do
          %Q(\n@import "rs_toolbar/rs_toolbar";)
        end
      end
    end
  end
end