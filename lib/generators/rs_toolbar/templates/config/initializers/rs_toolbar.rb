RsToolbar.setup do |config|
  # Current user instance wrapped with Proc
  config.user_class = proc { User.current }
end