#require 'rails/all'
require "rs_toolbar/engine"
#require 'rs_toolbar/controller_methods'
#require 'rs_toolbar/view_methods'
require 'rs_toolbar/toolbar'
#require 'rs_toolbar/actions_renderer'
require 'rs_toolbar/dsl_methods'

module RsToolbar
  #extend ActiveSupport::Autoload
  
  #autoload :ControllerMethods
  #autoload :Toolbar
  #autoload :ViewMethods
  #autoload :ActionsRenderer
  #autoload :DslMethods

  # User class
  mattr_accessor :user_class
  @@user_class = proc { User.current }

  # Proc to show toolbar
  mattr_accessor :show_case_proc
  @@show_case_proc = proc { User.current.present? }

  # Sign out route name
  mattr_accessor :sign_out_route_name
  @@sign_out_route_name = :destroy_user_session

  def self.setup
    yield self
  end
end

if defined?(Rails::Railtie)
  require 'rs_toolbar/railtie'
else
  require 'rs_toolbar/action_controller'
  require 'rs_toolbar/action_view'
  ::ActionController::Base.send :include, RsToolbar::ActionController
  ::ActionView::Base.send :include, RsToolbar::ActionView
end