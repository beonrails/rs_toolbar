# encoding: utf-8
require 'singleton'

module RsToolbar
  class DslMethods
    include Singleton

    ACTIONS_MAPPING = {
      :show  => proc { DslMethods.instance.entries! {
        new
        edit
        divide!
        index
      }},
      :index => proc { DslMethods.instance.entries! { new }},
      :new   => proc { DslMethods.instance.entries! { index }},
      :edit  => proc { DslMethods.instance.entries! {
        destroy
        show
        new
        divide!
        index
      }},
      :destroy => proc { }
    }

    attr_accessor :entries, :controller

    def initialize
      cleanup!
    end

    # Cleanup dropdown entries added in last request
    def cleanup!
      @entries = []
    end

    # Specify default entries
    def entries!(&block)
      instance_eval(&block) if block_given?
    end

    # Create custom menu entry
    def custom!(&block)
      put(&block)
    end

    def method_missing(m, *args, &block)
      if %w(new edit destroy show index).include?(m.to_s) && @controller.available_action?(m.to_s)
        put([m, args.first || {}])
      # TODO: Небольшой хак с проверкой наличия экшена index. Не рисуем разделитель, если
      # этот экшен не объявлен. Потом нужно придумать что-то лучше.
      elsif %w(divide!).include?(m.to_s) &&  @controller.available_action?(:index)
        put(nil)
      elsif @controller.available_action?(m)
        put([m, {}])
      end
    end

  protected

    def put(entry = nil, &block)
      if block_given?
        @entries << block
      elsif entry.nil?
        @entries << nil
      else
        put_or_merge(entry)
      end
    end

    def find(entry)
      @entries.detect { |x| x.is_a?(Enumerable) && x.first == entry.first.to_sym }
    end

    def exists?(entry)
      find(entry).present?
    end

    def put_or_merge(entry)
      if exists?(entry)
        # Merge entry options
        find(entry).last.deep_merge!(entry.last)
      else
        @entries << entry
      end
    end

  end
end