# encoding: utf-8

module RsToolbar
  class ActionsRenderer
    #include Rails.application.routes.url_helpers
    include ::ActionController::UrlFor
    include RsToolbar::Engine.routes.url_helpers
    include ::ActionView::Helpers::UrlHelper
    include ::ActionView::Helpers::TagHelper
    include ::ActionView::Context
    include ::ActionView::Helpers::TranslationHelper
    include ::ActionView::Helpers::OutputSafetyHelper

    ICON_MAP = {
      :show    => 'icon-eye-open',
      :index   => 'icon-list',
      :new     => 'icon-plus-sign',
      :edit    => 'icon-edit',
      :destroy => 'icon-trash'
    }

    attr_accessor :options, :controller

    delegate :env, :request, :to => :controller

    def initialize(controller)
      @controller = controller
      #send(:include, controller._routes)
    end

    # Render actions dropdown
    def render
      list = []
      
      @options[:entries].each do |entry|
        list << case entry
          when Proc
            render_custom_entry(&entry)
          when Enumerable
            render_entry(*entry)
          when NilClass
            render_divider
        end
      end

      #debugger

      raw(list.join)
    end

    def has_options?
      not @options.nil?
    end

  protected

    def render_entry(action_name, html_options)
      controller      = @options[:controller]
      controller_name = controller.controller_name
      i18n_controller_name = controller.class.to_s.underscore.sub('_controller', '').gsub('/', '_')
      content         = t("rs_toolbar.#{i18n_controller_name}.#{action_name}")

      # Prepare html options
      html_options = {
        :href => controller.view_context.url_for(
          :action     => action_name,
          :controller => controller_name
        )
      }.merge(html_options)

      # Handle destroy link
      if action_name == :destroy
        html_options['data-confirm'] = t('rs_toolbar.data_confirm'),
        html_options['data-method']  = :delete
      end

      # Build link and wrap with li tag
      link = content_tag :a, html_options do
        raw(%Q{<i class="icon #{ICON_MAP[action_name]}"></i>#{content}})
      end

      wrap_with_li(link)
    end

    def render_custom_entry(&block)
      wrap_with_li(block.bind(controller).call)
    end

    def render_divider
      wrap_with_li(nil)
    end

    def wrap_with_li(entry)
      if entry.nil?
        content_tag(:li, '', :class => :divider)
      else
        content_tag(:li, raw(entry))
      end
    end
  end
end