require 'rs_toolbar'
require 'rails'

module RsToolbar
  class Railtie < Rails::Railtie
    initializer "rs_toolbar.action_controller" do |app|
      if defined?(::ActionController)
        require 'rs_toolbar/action_controller'
        ::ActionController::Base.send :include, RsToolbar::ActionController
      end
    end
    
    initializer "rs_toolbar.action_view" do |app|
      require 'rs_toolbar/action_view'
      ::ActionView::Base.send :include, RsToolbar::ActionView
    end
  end
end