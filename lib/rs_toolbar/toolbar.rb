# encoding: utf-8

module RsToolbar
  # Этот класс определяет, какие элементы меню и с каким содержимым
  # мы будем отображать в выпадающем меню
  class Toolbar
    attr_accessor :controller

    def initialize(controller, &block)
      # Cleanup dropdown entries added in last request
      DslMethods.instance.cleanup!
      DslMethods.instance.controller = controller

      @controller = controller
      instance_eval(&block) if block_given?

      if DslMethods::ACTIONS_MAPPING.has_key?(@controller.action_name.to_sym)
        DslMethods::ACTIONS_MAPPING[@controller.action_name.to_sym].call
      end
#raise DslMethods.instance.entries.to_json
      @controller.instance_variable_set "@_rs_toolbar", {
        :controller => @controller,
        :entries    => DslMethods.instance.entries
      }
    end

    def method_missing(m, *args, &block)
      if @controller.respond_to?(m)
        if %w(show index new edit destroy).include?(@controller.action_name.to_s) && @controller.action_name.to_s == m.to_s
          DslMethods.instance.entries!(&block)
        end
      else
        super
      end
    end
  end
end