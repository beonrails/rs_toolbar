# encoding: utf-8

module RsToolbar
  # Набор view helpers
  module ActionView
    # Рендеринг меню действий
    def rs_toolbar_actions
      require 'rs_toolbar/actions_renderer'
      options  = instance_variable_get('@_rs_toolbar')

      return if options.nil?

      renderer = RsToolbar::ActionsRenderer.new(options[:controller])
      renderer.options = options
      renderer.render if renderer.has_options?
    end

    def rs_toolbar
      render :partial => 'rs_toolbar/toolbar'
    end

    def rs_toolbar_username
      RsToolbar.user_class.call.to_s
    end
  end
end