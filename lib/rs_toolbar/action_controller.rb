# encoding: utf-8

module RsToolbar
  module ActionController
    extend ActiveSupport::Concern

    module ClassMethods
      def rs_toolbar(options = {}, &block)
        before_filter(options) do |controller|
          RsToolbar::Toolbar.new(controller, &block)
        end
      end
    end
  end
end