$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "rs_toolbar/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "rs_toolbar"
  s.version     = RsToolbar::VERSION
  s.authors     = ["Andrew Kozlov"]
  s.email       = ["demerest@gmail.com"]
  s.homepage    = "http://gitlab.rocketscience.it/rs_toolbar"
  s.description = "Toolbar for users and admins"
  s.summary     = s.description

  s.files = Dir["{app,config,db,lib}/**/*"] + ["MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 3.2.6"
end
